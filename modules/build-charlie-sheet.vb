Rem Attribute VBA_ModuleType=VBAModule
Option VBASupport 1
Public Toggle_Option As Variant

' String manipulation is the key to good programming; thus: a string function... ingenious!
Public Function STR_SPLIT(str, sep, n) As String
    If str = "" Then
        Exit Function
    Else
        Dim V() As String
        V = Split(str, sep)
        STR_SPLIT = V(n - 1)
    End If
End Function

' Create a Worksheet with a name that depends on the captured date
Public Function Month_Sheet_Name(xMonth As Integer)
    Dim xName As String

    If Month(Date) < 10 Then
        Select Case xMonth
            Case 10: xName = "OCT" & " " & Mid(Year(Date) - 1, 3, 2)
            Case 11: xName = "NOV" & " " & Mid(Year(Date) - 1, 3, 2)
            Case 12: xName = "DEC" & " " & Mid(Year(Date) - 1, 3, 2)
            Case 1:  xName = "JAN" & " " & Mid(Year(Date), 3, 2)
            Case 2:  xName = "FEB" & " " & Mid(Year(Date), 3, 2)
            Case 3:  xName = "MAR" & " " & Mid(Year(Date), 3, 2)
            Case 4:  xName = "APR" & " " & Mid(Year(Date), 3, 2)
            Case 5:  xName = "MAY" & " " & Mid(Year(Date), 3, 2)
            Case 6:  xName = "JUN" & " " & Mid(Year(Date), 3, 2)
            Case 7:  xName = "JUL" & " " & Mid(Year(Date), 3, 2)
            Case 8:  xName = "AUG" & " " & Mid(Year(Date), 3, 2)
            Case 9:  xName = "SEP" & " " & Mid(Year(Date), 3, 2)
        End Select
    Else
        Select Case xMonth
            Case 10: xName = "OCT" & " " & Mid(Year(Date), 3, 2)
            Case 11: xName = "NOV" & " " & Mid(Year(Date), 3, 2)
            Case 12: xName = "DEC" & " " & Mid(Year(Date), 3, 2)
            Case 1:  xName = "JAN" & " " & Mid(Year(Date) + 1, 3, 2)
            Case 2:  xName = "FEB" & " " & Mid(Year(Date) + 1, 3, 2)
            Case 3:  xName = "MAR" & " " & Mid(Year(Date) + 1, 3, 2)
            Case 4:  xName = "APR" & " " & Mid(Year(Date) + 1, 3, 2)
            Case 5:  xName = "MAY" & " " & Mid(Year(Date) + 1, 3, 2)
            Case 6:  xName = "JUN" & " " & Mid(Year(Date) + 1, 3, 2)
            Case 7:  xName = "JUL" & " " & Mid(Year(Date) + 1, 3, 2)
            Case 8:  xName = "AUG" & " " & Mid(Year(Date) + 1, 3, 2)
            Case 9:  xName = "SEP" & " " & Mid(Year(Date) + 1, 3, 2)
        End Select
    End If
    Month_Sheet_Name = xName
End Function

' Capture all the values from the imported Unit Flights Spreadsheet of whichever month, and plug-in the values for each day
Public Sub Build_Charlie_Sheet()
    Dim Wkb As Workbook, Wks As Worksheet, Current_Sheet As Worksheet
    Dim Day_Night_Zeroes As Boolean, No_Nights As Boolean
    Dim Msg As String, Response As String, Unit_Flights As String, xDigit As String, xCell As String, xStr As String, _
        Date_Rng As String, Fiscal As String, Month_Name As String
    Dim xStart As Integer, xLast As Integer, xDay As Integer, Current_Month As Integer, Last_Month As Integer, Day_Sorties As Integer, _
        Day_Hours As Integer, Night_Sorties As Integer, Night_Hours As Integer, PAX As Integer, Cargo As Integer, Approaches As Integer, _
        Landings As Integer, i As Integer, j As Integer, k As Integer, Start_Margin As Integer, xFirst_Day As Integer
    Dim Temp As Variant, Temp1 As Variant, Borders_CONST() As Variant, Date_Column As Variant, Day_Column As Variant, _
        BUNO_Column As Variant, NAVFLIR_Column As Variant, Launch_Column As Variant, Land_Column As Variant, _
        Launch_ICAO_Column As Variant, Land_ICAO_Column As Variant, Total_Sorties_Column As Variant, Day_Sorties_Column As Variant, _
        Night_Sorties_Column As Variant, Total_Hours_Column As Variant, Day_Hours_Column As Variant, Night_Hours_Column As Variant, _
        PAX_Rng As Variant, Cargo_Rng As Variant, Approaches_Rng As Variant, Landings_Rng As Variant
    
    ' Import the Unit Flights File into this workbook
    Call Import_File
    
    ' Ensure that the flight was imported successfully
    If ThisWorkbook.Sheets(ThisWorkbook.Sheets.count).Name <> "Sheet1" Then: Exit Sub
    
    ' Pull out if there are any errors [that we don't expect] later on
    On Error GoTo Exit_Point
    
    ' Initialize the Below Variables before working with the Cells on the Active Worksheet
    Day_Night_Zeroes = False
    No_Nights = False
    Borders_CONST = Array(7, 8, 9, 10, 11, 12): Rem-- Handle borders easily with an array
    Set Wks = ThisWorkbook.Worksheets(ThisWorkbook.Sheets.count)
    Wks.Activate
    
    ' Begin Worksheet Transformation from "Unit Flights - Expanded" Report --- TO --- "Charlie Sheet"
    With Wks.Cells
            
        ' OBSOLETE -- Old Method for Removing Cancelled Flights
        '-------------------------------------------------------
        '
        'Do
        '    On Error Resume Next
        '    Temp = .Find("1N?").Row
        '    If Err = 91 Then
        '        Temp = ""
        '        Err = ""
        '    End If
        '    If Temp <> "" Then: Rows(Temp & ":" & Temp).Delete
        'Loop While Temp <> ""
        '
        'Do
        '    On Error Resume Next
        '    Temp = .Find("2N?").Row
        '    If Err = 91 Then
        '        Temp = ""
        '        Err = ""
        '    End If
        '    If Temp <> "" Then: Rows(Temp & ":" & Temp).Delete
        'Loop While Temp <> ""
        '
        'Do
        '    On Error Resume Next
        '    Temp = .Find("1O?").Row
        '    If Err = 91 Then
        '        Temp = ""
        '        Err = ""
        '    End If
        '    If Temp <> "" Then: Rows(Temp & ":" & Temp).Delete
        'Loop While Temp <> ""
        '
        'Do
        '    On Error Resume Next
        '    Temp = .Find("2O?").Row
        '    If Err = 91 Then
        '        Temp = ""
        '        Err = ""
        '    End If
        '    If Temp <> "" Then: Rows(Temp & ":" & Temp).Delete
        'Loop While Temp <> ""
        
        ' Delete all Cancelled Flights
        For i = 7 To 10000
            On Error Resume Next
            If .Range("F" & i) = "MV-22B" Then
                If .Range("G" & i) = 0 Then
                    Do
                        Rows(i & ":" & i).Delete
                        If .Range("F" & i) = "" Then: Exit For
                    Loop While .Range("G" & i) = 0
                End If
            Else: Exit For
            End If
        Next i

        ' Remove this Section -- Unit Information is not required on the Charlie Sheet
        On Error Resume Next
        Temp = .Find("Unit Information").Address
        If Err = 91 Then
            Temp = ""
            Err = ""
        End If
        If Temp <> "" Then
            .Range(.Find("Unit Information").Address).Select
            xStart = .Range(.Find("Unit Information").Address).Column
            xLast = xStart + ActiveCell.MergeArea.Columns.count - 1
            Columns(Chr(64 + xStart) & ":" & Chr(64 + xLast)).Delete
        End If
        
        ' Remove this Section -- Charlie Sheet doesn't need to be so detailed, as to include personnel information (yet! e.g. FOAB)
        On Error Resume Next
        Temp = .Find("Personnel Information").Address
        If Err = 91 Then
            Temp = ""
            Err = ""
        End If
        If Temp <> "" Then
            .Range(.Find("Personnel Information").Address).Select
            xStart = .Range(.Find("Personnel Information").Address).Column
            Columns(Chr(64 + xStart) & ":" & Chr(64 + xStart)).Select
            Selection.Delete
        End If
        
        ' Remove this Section -- May become useful later on [BUT NOT NOW] to determine SBTP Percentages per Catergory
        On Error Resume Next
        Temp = .Find("Flight Type").Address
        If Err = 91 Then
            Temp = ""
            Err = ""
        End If
        If Temp <> "" Then
            .Range(.Find("Flight Type").Address).Select
            xStart = .Range(.Find("Flight Type").Address).Column
            Columns(Chr(64 + xStart) & ":" & Chr(64 + xStart)).Select
            Selection.Delete
        End If
        
        ' Remove this field -- Will only be useful when utilized in conjunction with the "LZ_UTE_TRACKER"
        Do
            On Error Resume Next
            Temp = .Find("Remarks").Column
            
            If Temp >= 52 Then
                xStr = "B" & Chr(64 + Temp - 52)
            ElseIf Temp > 26 Then: xStr = "A" & Chr(64 + Temp - 26)
            Else: xStr = Chr(64 + Temp)
            End If
            If Err = 91 Then
                Temp = ""
                Err = ""
            End If
            If Temp <> "" Then: Columns(xStr & ":" & xStr).Delete
        Loop While Temp <> ""

        ' Remove this field -- All cancelled flights were already removed up to this point, making this field: OBSOLETE
        On Error Resume Next
        Temp = .Find("Cancel Description").Column
        
        If Temp > 26 Then
            xStr = "A" & Chr(64 + Temp - 26)
        Else: xStr = Chr(64 + Temp)
        End If
        If Err = 91 Then
            Temp = ""
            Err = ""
        End If
        If Temp <> "" Then: Columns(Chr(64 + Temp) & ":" & Chr(64 + Temp + 1)).Delete

        ' Remove this field -- We need Aircraft Data, NOT Weapons Data
        On Error Resume Next
        Temp = .Find("Cat Shots").Column
        
        If Temp > 26 Then
            xStr = "A" & Chr(64 + Temp - 26)
        Else: xStr = Chr(64 + Temp)
        End If
        If Err = 91 Then
            Temp = ""
            Err = ""
        End If
        If Temp <> "" Then: Columns(xStr & ":" & xStr).Delete

        ' Remove this field -- We need Aircraft Data, NOT Weapons Data
        Do
            On Error Resume Next
            Temp = .Find("Range?").Column
            
            If Temp > 26 Then
                xStr = "A" & Chr(64 + Temp - 26)
            Else: xStr = Chr(64 + Temp)
            End If
            If Err = 91 Then
                Temp = ""
                Err = ""
            End If
            If Temp <> "" Then: Columns(xStr & ":" & xStr).Delete
        Loop While Temp <> ""

        ' Remove this field -- We need Aircraft Data, NOT Weapons Data
        Do
            On Error Resume Next
            Temp = .Find("Weapon?").Column
            
            If Temp > 26 Then
                xStr = "A" & Chr(64 + Temp - 26)
            Else: xStr = Chr(64 + Temp)
            End If
            If Err = 91 Then
                Temp = ""
                Err = ""
            End If
            If Temp <> "" Then: Columns(xStr & ":" & xStr).Delete
        Loop While Temp <> ""

        ' Remove this field -- Confirmed Pax is what we need; NOT "Opportune"
        Do
            On Error Resume Next
            Temp = .Find("Opportune Pax").Column
            
            If Temp >= 52 Then
                xStr = "B" & Chr(64 + Temp - 52)
            ElseIf Temp > 26 Then: xStr = "A" & Chr(64 + Temp - 26)
            Else: xStr = Chr(64 + Temp)
            End If
            If Err = 91 Then
                Temp = ""
                Err = ""
            End If
            If Temp <> "" Then: Columns(xStr & ":" & xStr).Delete
        Loop While Temp <> ""
        
        ' Remove this field -- Confirmed Cargo is what we need; NOT "Opportune"
        Do
            On Error Resume Next
            Temp = .Find("Opportune Cargo").Column
            
            If Temp >= 52 Then
                xStr = "B" & Chr(64 + Temp - 52)
            ElseIf Temp > 26 Then: xStr = "A" & Chr(64 + Temp - 26)
            Else: xStr = Chr(64 + Temp)
            End If
            If Err = 91 Then
                Temp = ""
                Err = ""
            End If
            If Temp <> "" Then: Columns(xStr & ":" & xStr).Delete
        Loop While Temp <> ""

        ' Remove this field -- Don't know what it's for; thus: Can not be implemented in this tracker
        Do
            On Error Resume Next
            Temp = .Find("Supported").Column
            
            If Temp >= 52 Then
                xStr = "B" & Chr(64 + Temp - 52)
            ElseIf Temp > 26 Then: xStr = "A" & Chr(64 + Temp - 26)
            Else: xStr = Chr(64 + Temp)
            End If
            If Err = 91 Then
                Temp = ""
                Err = ""
            End If
            If Temp <> "" Then: Columns(xStr & ":" & xStr).Delete
        Loop While Temp <> ""
        
        ' Capture the first and last cell addresses (required for upcoming For Loop)
        .Range("A1").Select
        Date_Column = .Find("Date").Address
        xLast = .Range(Date_Column).End(xlDown).Row: Rem-- Last Row #
        xStart = .Range(Date_Column).Row + 1:        Rem-- First Row #
        Date_Column = .Range(Date_Column).Column
                        
        ' Condense Columns by utilizing the Sum Function: Execute on Approaches
        Temp = ""
        Temp = .Find("Approaches").Column
        
        ' There are no APPROACHES
        If Err = 91 Then
            Err = 0
            Temp = .Range("A6").End(xlToRight).Column
            If Temp > 1000 Then: Temp1 = .Range(Chr(64 + Temp + 1) & "6").End(xlToRight).End(xlToLeft).Column
            .Range(Chr(64 + Temp) & 6) = "APPROACHES"
            
        ' There are APPROACHES
        Else
            Columns(Chr(64 + Temp) & ":" & Chr(64 + Temp)).Insert
            Temp1 = .Find("Landings").Column
            
            ' There are APPROACHES, but no LANDINGS
            If Err = 91 Then
                Err = 0
                For j = xStart To xLast
                    With .Range(Chr(64 + Temp) & j)
                        If .Offset(0, 1) <> "" Or .Offset(0, 1) <> 0 Then: .Value = .Offset(0, 1) * 1
                    End With
                Next j
            Else
                ' Single Column Calculation
                If Temp1 > 1000 Then
                    
                    ' Convert the value into a number
                    For j = xStart To xLast
                        With .Range(Chr(64 + Temp) & j)
                            If .Offset(0, 1) <> "" Or .Offset(0, 1) <> 0 Then: .Value = .Offset(0, 1) * 1
                        End With
                    Next j
                    
                ' Multiple Column Calculation
                Else
                    ' Convert the area of numbers formatted as text, into numbers
                    For i = Temp + 1 To Temp1 - 1
                        For j = xStart To xLast
                            With .Range(Chr(64 + i) & j)
                                If .Value <> "" Or .Value <> 0 Then: .Value = .Value * 1
                            End With
                        Next j
                    Next i
                    For i = xStart To xLast
                        .Range(Chr(64 + Temp) & i).Formula = "=SUM(" & (Chr(64 + Temp + 1) & i & ":" & Chr(64 + Temp1 - 1) & i) & ")"
                        .Range(Chr(64 + Temp) & i).Value = .Range(Chr(64 + Temp) & i).Value * 1
                        If .Range(Chr(64 + Temp) & i) = "" Or .Range(Chr(64 + Temp) & i) = 0 Then: .Range(Chr(64 + Temp) & i) = 0
                    Next i
                End If
            End If
        
            ' Delete the cells used to create the sums
            If Err = 91 Then
                Err = 0
                Columns(Chr(64 + Temp + 1)).Delete
            Else
                If Abs(Temp - Temp1) > 2 Then
                    Columns(Chr(64 + Temp + 1) & ":" & Chr(64 + Temp1 - 1)).Delete
                Else: Columns(Chr(64 + Temp + 1)).Delete
                End If
            End If
            .Range(Chr(64 + Temp) & "6") = "APPROACHES"
        End If
        
        ' Condense Columns by utilizing the Sum Function: Execute on Landings
        Temp = ""
        Temp = .Find("Landings").Column
        
        ' There are no LANDINGS
        If Err = 91 Then
            Err = 0
            Temp = .Range("A6").End(xlToRight).Column
            .Range(Chr(64 + Temp) & 6) = "LANDINGS"
            
        ' There are LANDINGS
        Else
            Columns(Chr(64 + Temp) & ":" & Chr(64 + Temp)).Insert
            .Range(Chr(64 + Temp) & "6") = "LANDINGS"
            Temp1 = ""
            Temp1 = .Find("Confirmed Pax").Column
            
            If Err = 91 Then
                Err = 0
                Temp1 = ""
                If .Range("A6").End(xlToRight).Column <> Temp + 1 Then: Temp1 = .Range("A6").End(xlToRight).Column + 1
            End If
            
            ' There are LANDINGS, but no PAX
            If Temp1 = "" Then
                For j = xStart To xLast
                    With .Range(Chr(64 + Temp) & j)
                        If .Offset(0, 1) <> "" Or .Offset(0, 1) <> 0 Then: .Value = .Offset(0, 1) * 1
                    End With
                Next j
            Else
                ' Single Column Calculation
                If Temp1 > 1000 Then
                    
                    ' Convert the value into a number
                    For k = Temp To Temp1
                        For j = xStart To xLast
                            xStr = Chr(64 + k) & j
                            With .Range(xStr)
                                If .Offset(0, 1) <> "" Or .Offset(0, 1) <> 0 Then: .Value = .Offset(0, 1) * 1
                            End With
                        Next j
                    Next k
                    
                ' Multiple Column Calculation
                Else
                    ' Convert the area of numbers formatted as text, into numbers
                    For i = Temp + 1 To Temp1 - 1
                        
                        ' Determine the actual column string
                        If i > 26 Then
                            xStr = "A" & Chr(64 + i - 26)
                        Else: xStr = Chr(64 + i)
                        End If
                        
                        ' Convert the value into a number
                        For j = xStart To xLast
                            With .Range(xStr & j)
                                If .Value <> "" Or .Value <> 0 Then: .Value = .Value * 1
                            End With
                        Next j
                    Next i
                    
                    ' Sum up the values for each type of Landings
                    For i = xStart To xLast
                    
                        ' Determine the actual column string
                        If Temp1 - 1 > 26 Then
                            xStr = "A" & Chr(64 + Temp1 - 1 - 26)
                        Else: xStr = Chr(64 + Temp1 - 1)
                        End If
                        
                        ' Enter the Sum into the new column
                        .Range(Chr(64 + Temp) & i).Formula = "=SUM(" & (Chr(64 + Temp + 1) & i & ":" & xStr & i) & ")"
                        .Range(Chr(64 + Temp) & i).Value = .Range(Chr(64 + Temp) & i).Value * 1
                        If .Range(Chr(64 + Temp) & i) = "" Or .Range(Chr(64 + Temp) & i) = 0 Then: .Range(Chr(64 + Temp) & i) = 0
                    Next i
                End If
            End If

            ' Delete the cells used to create the sums
            Temp1 = ""
            Temp1 = .Find("Confirmed Pax").Column
            
            If Err = 91 Then
                Err = 0
                Temp1 = ""
                Temp1 = .Range(Chr(64 + Temp + 1) & "6").End(xlToRight).Column
                If Temp1 > 1000 Then: Temp1 = .Range(Chr(64 + Temp + 1) & "6").End(xlToRight).End(xlToLeft).Column
            End If
            If Temp1 - 1 > 26 Then
                xStr = "A" & Chr(64 + Temp1 - 26)
            Else: xStr = Chr(64 + Temp1)
            End If
            If Temp1 = "" Then
                Err = 0
                Columns(Chr(64 + Temp + 1)).Select
                Selection.Delete
            Else: Columns(Chr(64 + Temp + 1) & ":" & xStr).Delete
            End If
        End If
            
        ' Condense Columns by utilizing the Sum Function: Execute on Confirmed Pax
        Temp = ""
        Temp = .Find("Confirmed Pax").Column
        
        ' There are no PAX
        If Err = 91 Then
            Err = 0
            Temp = .Range("A6").End(xlToRight).Column + 1
            .Range(Chr(64 + Temp) & 6) = "PAX"
            
        ' There is PAX
        Else
            Columns(Chr(64 + Temp) & ":" & Chr(64 + Temp)).Insert
            .Range(Chr(64 + Temp) & "6") = "PAX"
            Temp1 = ""
            Temp1 = .Find("Confirmed Cargo").Column
            If Err = 91 Then
                Temp1 = ""
                If .Range("A6").End(xlToRight).Column <> Temp + 1 Then: Temp1 = .Range("A6").End(xlToRight).Column + 1
            End If
            
            ' There's PAX, but no Cargo
            If Temp1 = "" Then
                For j = xStart To xLast
                    With .Range(Chr(64 + Temp) & j)
                        If .Offset(0, 1) <> "" Or .Offset(0, 1) <> 0 Then: .Value = .Offset(0, 1) * 1
                    End With
                Next j
            Else
                ' Single Column Calculation
                If Temp1 > 1000 Then
                    
                    ' Convert the value into a number
                    For j = xStart To xLast
                        With .Range(Chr(64 + Temp) & j)
                            If .Offset(0, 1) <> "" Or .Offset(0, 1) <> 0 Then: .Value = .Offset(0, 1) * 1
                        End With
                    Next j
                    
                ' Multiple Column Calculation
                Else
                    ' Convert the area of numbers formatted as text, into numbers
                    For i = Temp + 1 To Temp1 - 1
                        
                        ' Determine the actual column string
                        If i > 26 Then
                            xStr = "A" & Chr(64 + i - 26)
                        Else: xStr = Chr(64 + i)
                        End If
                        
                        ' Convert the value into a number
                        For j = xStart To xLast
                            With .Range(xStr & j)
                                If .Value <> "" Or .Value <> 0 Then: .Value = .Value * 1
                            End With
                        Next j
                    Next i
                    
                    ' Sum up the values for each type of PAX
                    For i = xStart To xLast
                    
                        ' Determine the actual column string
                        If Temp1 > 26 Then
                            xStr = "A" & Chr(64 + Temp1 - 26)
                        Else: xStr = Chr(64 + Temp1 - 1)
                        End If
                        
                        ' Enter the Sum into the new column
                        .Range(Chr(64 + Temp) & i).Formula = "=SUM(" & (Chr(64 + Temp + 1) & i & ":" & xStr & i) & ")"
                        .Range(Chr(64 + Temp) & i).Value = .Range(Chr(64 + Temp) & i).Value * 1
                        If .Range(Chr(64 + Temp) & i) = "" Or .Range(Chr(64 + Temp) & i) = 0 Then: .Range(Chr(64 + Temp) & i) = 0
                    Next i
                End If
            End If
            
            ' Delete the cells used to create the sums
            Temp1 = ""
            Temp1 = .Find("Confirmed Cargo").Column
            
            If Err = 91 Then
                Err = 0
                Temp1 = ""
                Temp1 = .Range(Chr(64 + Temp + 1) & "6").End(xlToRight).Column
                If Temp1 > 1000 Then: Temp1 = .Range(Chr(64 + Temp + 1) & "6").End(xlToRight).End(xlToLeft).Column + 1
            End If
            
            If Temp1 = "" Then
                Err = 0
                Columns(Chr(64 + Temp + 1)).Select
                Selection.Delete
            Else: Columns(Chr(64 + Temp + 1) & ":" & Chr(64 + Temp1 - 1)).Delete
            End If
        End If
        
        ' Condense Columns by utilizing the Sum Function: Execute on Confirmed Cargo
        Temp = ""
        Temp = .Find("Confirmed Cargo").Column
        
        ' There is no CARGO
        If Err = 91 Then
            Err = 0
            Temp = .Range("A6").End(xlToRight).Column + 1
            .Range(Chr(64 + Temp) & 6) = "CARGO"
            
        ' There is CARGO
        Else
            Columns(Chr(64 + Temp) & ":" & Chr(64 + Temp)).Insert
            Temp1 = .Range(.Find("Confirmed Cargo").Address).Offset(1, 0).End(xlToRight).Column
            
            ' Single Column Calculation
            If Temp1 > 1000 Then
                
                ' Convert the value into a number
                For j = xStart To xLast
                    With .Range(Chr(64 + Temp) & j)
                        If .Offset(0, 1) <> "" Or .Offset(0, 1) <> 0 Then: .Value = .Offset(0, 1) * 1
                    End With
                Next j
                    
            ' Multiple Column Calculation
            Else
                ' Convert the area of numbers formatted as text, into numbers
                For i = Temp + 1 To Temp1
                    
                    ' Determine the actual column string
                    If i > 26 Then
                        xStr = "A" & Chr(64 + i - 26)
                    Else: xStr = Chr(64 + i)
                    End If
                    
                    ' Convert the value into a number
                    For j = xStart To xLast
                        With .Range(xStr & j)
                            If .Value <> "" Or .Value <> 0 Then: .Value = .Value * 1
                        End With
                    Next j
                Next i
                
                ' Sum up the values for each type of Cargo
                For i = xStart To xLast
                
                    ' Determine the actual column string
                    If Temp1 > 26 Then
                        xStr = "A" & Chr(64 + Temp1 - 26)
                    Else: xStr = Chr(64 + Temp1)
                    End If
                    
                    ' Enter the Sum into the new column
                    .Range(Chr(64 + Temp) & i).Formula = "=SUM(" & (Chr(64 + Temp + 1) & i & ":" & xStr & i) & ")"
                    .Range(Chr(64 + Temp) & i).Value = .Range(Chr(64 + Temp) & i).Value * 1
                    If .Range(Chr(64 + Temp) & i) = "" Or .Range(Chr(64 + Temp) & i) = 0 Then: .Range(Chr(64 + Temp) & i) = 0
                Next i
            End If
            
            ' Delete the cells used to create the sums
            If Err = 91 Then
                Err = 0
                Columns(Chr(64 + Temp + 1)).Delete
            Else: Columns(Chr(64 + Temp + 1) & ":" & xStr).Delete
            End If
            .Range(Chr(64 + Temp) & "6") = "CARGO"
        End If

        ' Remove the extra rows above the header
        Rows("1:5").Delete
        
        ' Define Column References
        Date_Column = Chr(64 + Date_Column)
        BUNO_Column = Chr(64 + .Find("BUNO").Column)
        NAVFLIR_Column = Chr(64 + .Find("NAVFLIR ID").Column)
        Launch_Column = Chr(64 + .Find("Launch").Column)
        Land_Column = Chr(64 + .Find("Land").Column)
        Launch_ICAO_Column = Chr(64 + .Find("Launch ICAO").Column)
        Land_ICAO_Column = Chr(64 + .Find("Land ICAO").Column)
        Total_Sorties_Column = Chr(64 + .Find("Srt").Column)
        Total_Hours_Column = Chr(64 + .Find("TFT").Column)
        Night_Hours_Column = Chr(64 + .Find("ACMDR Night").Column)
        PAX_Rng = Chr(64 + .Find("PAX").Column)
        Cargo_Rng = Chr(64 + .Find("CARGO").Column)
        
        ' Recalculate what is the last row
        Date_Column = .Find("Date").Address
        xLast = .Range(Date_Column).End(xlDown).Row: Rem-- Last Row #
        xStart = .Range(Date_Column).Row + 1:        Rem-- First Row #
        Date_Column = .Range(Date_Column).Column
        Date_Column = Chr(64 + Date_Column)
        
        ' Format the rest of the worksheet
        For i = xStart To xLast
            .Range(BUNO_Column & i) = .Range(BUNO_Column & i) * 1
            .Range(Total_Sorties_Column & i) = .Range(Day_Sorties_Column & i) * 1
            .Range(Total_Hours_Column & i) = .Range(Total_Hours_Column & i) * 1
            .Range(Night_Hours_Column & i) = .Range(Night_Hours_Column & i) * 1
        Next i
        
        ' Insert a new column for the Day #'s
        Columns(Launch_Column & ":" & Launch_Column).Insert
        .Range(Launch_Column & 1) = "DAY"
        Day_Column = Chr(64 + .Find("DAY").Column)
        For i = xStart To xLast
            .Range(Day_Column & i).Formula = "=MID(" & Date_Column & i & ", 4, 2)"
            .Range(Day_Column & i) = .Range(Day_Column & i) * 1
        Next i
        
        ' Re-define Column References
        Date_Column = Chr(64 + .Find("Date").Column)
        BUNO_Column = Chr(64 + .Find("BUNO").Column)
        NAVFLIR_Column = Chr(64 + .Find("NAVFLIR ID").Column)
        Launch_Column = Chr(64 + .Find("Launch").Column)
        Land_Column = Chr(64 + .Find("Land").Column)
        Launch_ICAO_Column = Chr(64 + .Find("Launch ICAO").Column)
        Land_ICAO_Column = Chr(64 + .Find("Land ICAO").Column)
        Total_Sorties_Column = Chr(64 + .Find("Srt").Column)
        Total_Hours_Column = Chr(64 + .Find("TFT").Column)
        Night_Hours_Column = Chr(64 + .Find("ACMDR Night").Column)
        PAX_Rng = Chr(64 + .Find("PAX").Column)
        Cargo_Rng = Chr(64 + .Find("CARGO").Column)
        
        ' Sort each flight event by day #
        Wks.Sort.SortFields.Clear
        Wks.Sort.SortFields.Add Key:=Range(Day_Column & xStart & ":" & Day_Column & xLast), _
                                SortOn:=xlSortOnValues, Order:=xlAscending, DataOption:=xlSortNormal
        With Wks.Sort
            .SetRange Range("A1:" & Cargo_Rng & xLast)
            .Header = xlYes
            .MatchCase = False
            .Orientation = xlTopToBottom
            .SortMethod = xlPinYin
            .Apply
        End With
        
        ' Insert a new column for the day hours
        Temp = .Range(.Find("TFT").Address).Column
        Columns(Chr(64 + Temp) & ":" & Chr(64 + Temp)).Insert
        .Range(.Find("TFT").Address).Offset(0, -1) = "HRS-D"
        Day_Hours_Column = Chr(64 + .Find("HRS-D").Column)
        If Night_Hours_Column = Empty Then
            Err = 0
            No_Nights = True
            Night_Hours_Column = Chr(64 + .Find("TFT").Column)
            Columns(Night_Hours_Column & ":" & Night_Hours_Column).Insert
            .Range(.Find("TFT").Address).Offset(0, -1) = "HRS-N"
        Else
            .Range(.Find("ACMDR Night").Address) = "HRS-N"
            Night_Hours_Column = Chr(64 + .Find("HRS-N").Column)
        End If
        
        ' Re-define Column References
        Date_Column = Chr(64 + .Find("Date").Column)
        Day_Column = Chr(64 + .Find("DAY").Column)
        BUNO_Column = Chr(64 + .Find("BUNO").Column)
        NAVFLIR_Column = Chr(64 + .Find("NAVFLIR ID").Column)
        Launch_Column = Chr(64 + .Find("Launch").Column)
        Land_Column = Chr(64 + .Find("Land").Column)
        Launch_ICAO_Column = Chr(64 + .Find("Launch ICAO").Column)
        Land_ICAO_Column = Chr(64 + .Find("Land ICAO").Column)
        Total_Sorties_Column = Chr(64 + .Find("Srt").Column)
        Total_Hours_Column = Chr(64 + .Find("TFT").Column)
        Day_Hours_Column = Chr(64 + .Find("HRS-D").Column)
        Night_Hours_Column = Chr(64 + .Find("HRS-N").Column)
        PAX_Rng = Chr(64 + .Find("PAX").Column)
        Cargo_Rng = Chr(64 + .Find("CARGO").Column)
        
        For i = xStart To xLast
            .Range(Day_Hours_Column & i).Formula = "=ABS(" & Total_Hours_Column & i & " - " & Night_Hours_Column & i & ")"
            .Range(Day_Hours_Column & i) = .Range(Day_Hours_Column & i) * 1
        Next i
        Columns(Total_Hours_Column & ":" & Total_Hours_Column).Delete
        
        ' Re-define Column References
        Date_Column = Chr(64 + .Find("Date").Column)
        Day_Column = Chr(64 + .Find("DAY").Column)
        BUNO_Column = Chr(64 + .Find("BUNO").Column)
        NAVFLIR_Column = Chr(64 + .Find("NAVFLIR ID").Column)
        Launch_Column = Chr(64 + .Find("Launch").Column)
        Land_Column = Chr(64 + .Find("Land").Column)
        Launch_ICAO_Column = Chr(64 + .Find("Launch ICAO").Column)
        Land_ICAO_Column = Chr(64 + .Find("Land ICAO").Column)
        Total_Sorties_Column = Chr(64 + .Find("Srt").Column)
        Day_Hours_Column = Chr(64 + .Find("HRS-D").Column)
        Night_Hours_Column = Chr(64 + .Find("HRS-N").Column)
        PAX_Rng = Chr(64 + .Find("PAX").Column)
        Cargo_Rng = Chr(64 + .Find("CARGO").Column)
        
        ' Insert new columns for the day/night sorties
        Columns(Date_Column & ":" & Date_Column).Insert
        .Range(Date_Column & 1) = "SRT-D"
        Date_Column = Chr(64 + .Find("Date").Column)
        Columns(Date_Column & ":" & Date_Column).Insert
        .Range(Date_Column & 1) = "SRT-N"
        
        ' Re-define Column References
        Date_Column = Chr(64 + .Find("Date").Column)
        Day_Column = Chr(64 + .Find("DAY").Column)
        BUNO_Column = Chr(64 + .Find("BUNO").Column)
        NAVFLIR_Column = Chr(64 + .Find("NAVFLIR ID").Column)
        Launch_Column = Chr(64 + .Find("Launch").Column)
        Land_Column = Chr(64 + .Find("Land").Column)
        Launch_ICAO_Column = Chr(64 + .Find("Launch ICAO").Column)
        Land_ICAO_Column = Chr(64 + .Find("Land ICAO").Column)
        Total_Sorties_Column = Chr(64 + .Find("Srt").Column)
        Day_Sorties_Column = Chr(64 + .Find("SRT-D").Column)
        Night_Sorties_Column = Chr(64 + .Find("SRT-N").Column)
        Day_Hours_Column = Chr(64 + .Find("HRS-D").Column)
        Night_Hours_Column = Chr(64 + .Find("HRS-N").Column)
        PAX_Rng = Chr(64 + .Find("PAX").Column)
        Cargo_Rng = Chr(64 + .Find("CARGO").Column)
        
        ' Divide the Sorties amongst the "SRT-D" & "SRT-N" Columns, based on the "HRS-D" & "HRS-N" Ratio
        If No_Nights = True Then
            For i = xStart To xLast
                .Range(Day_Sorties_Column & i) = .Range(Total_Sorties_Column & i)
            Next i
        Else
            For i = xStart To xLast
            
                ' Determine if any Day or Night Hours Cell contains a zero or not
                Day_Night_Zeroes = False
                If .Range(Day_Hours_Column & i) = 0 Then: Day_Night_Zeroes = True
                If .Range(Night_Hours_Column & i) = 0 Then: Day_Night_Zeroes = True
                
                ' If any Day or Night Hours Cell contains zero hours, then one cell gets the total sorties while the other gets none
                If Day_Night_Zeroes = True Then
                    
                    ' Total Sorties goes to "HRS-N"
                    If .Range(Day_Hours_Column & i) = 0 Then
                        .Range(Day_Sorties_Column & i) = 0
                        .Range(Night_Sorties_Column & i) = .Range(Total_Sorties_Column & i)
                        
                    ' Total Sorties goes to "HRS-D"
                    Else
                        .Range(Night_Sorties_Column & i) = 0
                        .Range(Day_Sorties_Column & i) = .Range(Total_Sorties_Column & i)
                    End If
                Else
                    ' Day Hours is greater than Night Hours, so ensure that Day Sorties is rounded-down to prevent it from creating an extra sortie
                    If .Range(Day_Hours_Column & i) > .Range(Night_Hours_Column & i) Then
                        If .Range(Day_Hours_Column & i) > .Range(Night_Hours_Column & i) Then
                            .Range(Day_Sorties_Column & i) = .Application.WorksheetFunction.RoundUp(.Range(Total_Sorties_Column & i) * (.Range(Day_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                            .Range(Night_Sorties_Column & i) = .Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) * (.Range(Night_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                            
                            ' Ensure there are no zero values from the previous division operations
                            If .Range(Day_Sorties_Column & i) = 0 Then
                                .Range(Day_Sorties_Column & i) = .Application.WorksheetFunction.RoundUp(.Range(Total_Sorties_Column & i) * (.Range(Day_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                                .Range(Night_Sorties_Column & i) = .Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) * (.Range(Night_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                            ElseIf .Range(Night_Sorties_Column & i) = 0 Then
                                .Range(Night_Sorties_Column & i) = .Application.WorksheetFunction.RoundUp(.Range(Total_Sorties_Column & i) * (.Range(Night_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                                .Range(Day_Sorties_Column & i) = .Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) * (.Range(Day_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                            End If
                        Else
                            .Range(Day_Sorties_Column & i) = .Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) * (.Range(Day_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                            .Range(Night_Sorties_Column & i) = .Application.WorksheetFunction.RoundUp(.Range(Total_Sorties_Column & i) * (.Range(Night_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                        
                            ' Ensure there are no zero values from the previous division operations
                            If .Range(Day_Sorties_Column & i) = 0 Then
                                .Range(Day_Sorties_Column & i) = .Application.WorksheetFunction.RoundUp(.Range(Total_Sorties_Column & i) * (.Range(Day_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                                .Range(Night_Sorties_Column & i) = .Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) * (.Range(Night_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                            ElseIf .Range(Night_Sorties_Column & i) = 0 Then
                                .Range(Night_Sorties_Column & i) = .Application.WorksheetFunction.RoundUp(.Range(Total_Sorties_Column & i) * (.Range(Night_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                                .Range(Day_Sorties_Column & i) = .Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) * (.Range(Day_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                            End If
                        End If
                        
                        ' Let's handle decimal values properly
                        If .Range(Total_Sorties_Column & i) <> 1 Then
                            If .Range(Night_Sorties_Column & i) = 0 Then
                                .Range(Night_Sorties_Column & i) = 1
                                .Range(Day_Sorties_Column & i) = .Range(Day_Sorties_Column & i) - 1
                            End If
                        End If
                        
                        ' When Day hours and Night hours are equivalent, then ratios can't be used-- divide each value by half instead
                        If .Range(Day_Sorties_Column & i) = .Range(Night_Sorties_Column & i) Then
                        
                            ' If the Total sorties are not even, then you can NOT just divide by half, for the ratio will be off by one
                            If Application.WorksheetFunction.Even(.Range(Total_Sorties_Column & i)) = .Range(Total_Sorties_Column & i) Then
                                .Range(Day_Sorties_Column & i) = .Range(Total_Sorties_Column & i) / 2
                                .Range(Night_Sorties_Column & i) = .Range(Total_Sorties_Column & i) / 2
                                
                            ' The Total sorties are not even; the extra sortie will go to whichever time gained the most hours
                            Else
                                If .Range(Day_Hours_Column & i) > .Range(Night_Hours_Column & i) Then
                                    .Range(Day_Sorties_Column & i) = Application.WorksheetFunction.Round(.Range(Total_Sorties_Column & i) / 2, 0)
                                    .Range(Night_Sorties_Column & i) = Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) / 2, 0)
                                Else
                                    .Range(Day_Sorties_Column & i) = Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) / 2, 0)
                                    .Range(Night_Sorties_Column & i) = Application.WorksheetFunction.Round(.Range(Total_Sorties_Column & i) / 2, 0)
                                End If
                            End If
                        End If
                    ' Night Hours is greater than Day Hours, so ensure that Day Sorties is rounded-down to prevent it from creating an extra sortie
                    Else
                        If .Range(Day_Hours_Column & i) > .Range(Night_Hours_Column & i) Then
                            .Range(Day_Sorties_Column & i) = .Application.WorksheetFunction.RoundUp(.Range(Total_Sorties_Column & i) * (.Range(Day_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                            .Range(Night_Sorties_Column & i) = .Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) * (.Range(Night_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                        
                            ' Ensure there are no zero values from the previous division operations
                            If .Range(Day_Sorties_Column & i) = 0 Then
                                .Range(Day_Sorties_Column & i) = .Application.WorksheetFunction.RoundUp(.Range(Total_Sorties_Column & i) * (.Range(Day_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                                .Range(Night_Sorties_Column & i) = .Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) * (.Range(Night_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                            ElseIf .Range(Night_Sorties_Column & i) = 0 Then
                                .Range(Night_Sorties_Column & i) = .Application.WorksheetFunction.RoundUp(.Range(Total_Sorties_Column & i) * (.Range(Night_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                                .Range(Day_Sorties_Column & i) = .Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) * (.Range(Day_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                            End If
                        Else
                            .Range(Day_Sorties_Column & i) = .Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) * (.Range(Day_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                            .Range(Night_Sorties_Column & i) = .Application.WorksheetFunction.RoundUp(.Range(Total_Sorties_Column & i) * (.Range(Night_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                            
                            ' Ensure there are no zero values from the previous division operations
                            If .Range(Day_Sorties_Column & i) = 0 Then
                                .Range(Day_Sorties_Column & i) = .Application.WorksheetFunction.RoundUp(.Range(Total_Sorties_Column & i) * (.Range(Day_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                                .Range(Night_Sorties_Column & i) = .Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) * (.Range(Night_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                            ElseIf .Range(Night_Sorties_Column & i) = 0 Then
                                .Range(Night_Sorties_Column & i) = .Application.WorksheetFunction.RoundUp(.Range(Total_Sorties_Column & i) * (.Range(Night_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                                .Range(Day_Sorties_Column & i) = .Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) * (.Range(Day_Hours_Column & i) / (.Range(Day_Hours_Column & i) + .Range(Night_Hours_Column & i))), 0)
                            End If
                        End If
                        
                        ' Let's handle decimal values properly
                        If .Range(Total_Sorties_Column & i) * 1 = 1 Then
                            If .Range(Day_Sorties_Column & i) = 0 Then
                                .Range(Day_Sorties_Column & i) = 1
                                .Range(Night_Sorties_Column & i) = .Range(Day_Sorties_Column & i) - 1
                                ElseIf .Range(Night_Sorties_Column & i) = 0 Then
                                    .Range(Night_Sorties_Column & i) = 1
                                    .Range(Day_Sorties_Column & i) = .Range(Night_Sorties_Column & i) - 1
                            End If
                        End If
                        
                        ' When Day hours and Night hours are equivalent, then ratios can't be used-- divide each value by half instead
                        If .Range(Day_Sorties_Column & i) = .Range(Night_Sorties_Column & i) Then
                        
                            ' If the Total sorties are not even, then you can NOT just divide by half, for the ratio will be off by one
                            If Application.WorksheetFunction.Even(.Range(Total_Sorties_Column & i)) = .Range(Total_Sorties_Column & i) Then
                                .Range(Day_Sorties_Column & i) = .Range(Total_Sorties_Column & i) / 2
                                .Range(Night_Sorties_Column & i) = .Range(Total_Sorties_Column & i) / 2
                                
                            ' The Total sorties are not even; the extra sortie will go to whichever time gained the most hours
                            Else
                                If .Range(Day_Hours_Column & i) > .Range(Night_Hours_Column & i) Then
                                    .Range(Day_Sorties_Column & i) = Application.WorksheetFunction.Round(.Range(Total_Sorties_Column & i) / 2, 0)
                                    .Range(Night_Sorties_Column & i) = Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) / 2, 0)
                                Else
                                    .Range(Day_Sorties_Column & i) = Application.WorksheetFunction.RoundDown(.Range(Total_Sorties_Column & i) / 2, 0)
                                    .Range(Night_Sorties_Column & i) = Application.WorksheetFunction.Round(.Range(Total_Sorties_Column & i) / 2, 0)
                                End If
                            End If
                        End If
                    End If
                End If
                
                ' Ensure the values are converted from text to numbers
                .Range(Day_Sorties_Column & i) = .Range(Day_Sorties_Column & i) * 1
                .Range(Night_Sorties_Column & i) = .Range(Night_Sorties_Column & i) * 1
            Next i
        End If
        Columns(Total_Sorties_Column & ":" & Total_Sorties_Column).Delete
        
        ' Re-define Column References
        Date_Column = Chr(64 + .Find("Date").Column)
        Day_Column = Chr(64 + .Find("DAY").Column)
        BUNO_Column = Chr(64 + .Find("BUNO").Column)
        NAVFLIR_Column = Chr(64 + .Find("NAVFLIR ID").Column)
        Launch_Column = Chr(64 + .Find("Launch").Column)
        Land_Column = Chr(64 + .Find("Land").Column)
        Launch_ICAO_Column = Chr(64 + .Find("Launch ICAO").Column)
        Land_ICAO_Column = Chr(64 + .Find("Land ICAO").Column)
        Total_Sorties_Column = Chr(64 + .Find("Srt").Column)
        Day_Sorties_Column = Chr(64 + .Find("SRT-D").Column)
        Night_Sorties_Column = Chr(64 + .Find("SRT-N").Column)
        Day_Hours_Column = Chr(64 + .Find("HRS-D").Column)
        Night_Hours_Column = Chr(64 + .Find("HRS-N").Column)
        PAX_Rng = Chr(64 + .Find("PAX").Column)
        Cargo_Rng = Chr(64 + .Find("CARGO").Column)
               
        ' Make space for the "DAY" Column
        Columns("A:A").Insert
        
        ' Re-define Column References
        Date_Column = Chr(64 + .Find("Date").Column)
        Day_Column = Chr(64 + .Find("DAY").Column)
        BUNO_Column = Chr(64 + .Find("BUNO").Column)
        NAVFLIR_Column = Chr(64 + .Find("NAVFLIR ID").Column)
        Launch_Column = Chr(64 + .Find("Launch").Column)
        Land_Column = Chr(64 + .Find("Land").Column)
        Launch_ICAO_Column = Chr(64 + .Find("Launch ICAO").Column)
        Land_ICAO_Column = Chr(64 + .Find("Land ICAO").Column)
        Total_Sorties_Column = Chr(64 + .Find("Srt").Column)
        Day_Sorties_Column = Chr(64 + .Find("SRT-D").Column)
        Night_Sorties_Column = Chr(64 + .Find("SRT-N").Column)
        Day_Hours_Column = Chr(64 + .Find("HRS-D").Column)
        Night_Hours_Column = Chr(64 + .Find("HRS-N").Column)
        PAX_Rng = Chr(64 + .Find("PAX").Column)
        Cargo_Rng = Chr(64 + .Find("CARGO").Column)
        
        ' Place the the "DAY" Column in front of the TMS Column
        Columns(Day_Column & ":" & Day_Column).Select
        Selection.Cut
        Columns("A:A").Select
        ActiveSheet.Paste
        Columns(Day_Column & ":" & Day_Column).Delete
        
        ' Create space for a header
        Rows("1:1").Insert
        
        ' Re-define Column References
        Date_Column = Chr(64 + .Find("Date").Column)
        Day_Column = Chr(64 + .Find("DAY").Column)
        BUNO_Column = Chr(64 + .Find("BUNO").Column)
        NAVFLIR_Column = Chr(64 + .Find("NAVFLIR ID").Column)
        Launch_Column = Chr(64 + .Find("Launch").Column)
        Land_Column = Chr(64 + .Find("Land").Column)
        Launch_ICAO_Column = Chr(64 + .Find("Launch ICAO").Column)
        Land_ICAO_Column = Chr(64 + .Find("Land ICAO").Column)
        Total_Sorties_Column = Chr(64 + .Find("Srt").Column)
        Day_Sorties_Column = Chr(64 + .Find("SRT-D").Column)
        Night_Sorties_Column = Chr(64 + .Find("SRT-N").Column)
        Day_Hours_Column = Chr(64 + .Find("HRS-D").Column)
        Night_Hours_Column = Chr(64 + .Find("HRS-N").Column)
        PAX_Rng = Chr(64 + .Find("PAX").Column)
        Cargo_Rng = Chr(64 + .Find("CARGO").Column)
                
        ' Rename the worksheet with the appropriate month name
        Current_Month = STR_SPLIT(.Range(Date_Column & i), "/", 1)
        Month_Name = Month_Sheet_Name(Current_Month)
        ThisWorkbook.Sheets(ThisWorkbook.Sheets.count).Name = Month_Name
        Set Current_Sheet = ThisWorkbook.Worksheets(ThisWorkbook.Sheets.count)
        
        ' Create a header for the new sheet
        Current_Sheet.Range("A1:S1").Merge
        Current_Sheet.Range("A1:S1").Interior.Color = vbRed
        Current_Sheet.Range("A1:S1").Font.Color = vbBlue
        Current_Sheet.Range("A1:S1").Font.Size = 8
        Current_Sheet.Range("A1:S1").Font.Bold = True
        Current_Sheet.Range("A1:AB1").HorizontalAlignment = xlCenter
        Current_Sheet.Range("A1:AB1").VerticalAlignment = xlCenter
        Current_Sheet.Range("A1:S1") = "ACTUAL FLIGHTS"
        Current_Sheet.Range("A2:S2").Interior.Color = vbRed
        Current_Sheet.Range("A2:S2").Font.Color = vbBlue
        Current_Sheet.Range("A2:S2").Font.Size = 8
        Current_Sheet.Range("A2:S2").Font.Bold = True
        Current_Sheet.Range("A2:AB2").HorizontalAlignment = xlCenter
        Current_Sheet.Range("A2:AB2").VerticalAlignment = xlCenter
        Current_Sheet.Range("T1:AB1").Merge
        Current_Sheet.Range("T1:AB1").Interior.ColorIndex = 31
        Current_Sheet.Range("T1:T33").Interior.ColorIndex = 31
        Current_Sheet.Range("T1:T33").Font.Color = vbBlue
        Current_Sheet.Range("T1:T33").Font.Size = 8
        Current_Sheet.Range("T1:T33").Font.Bold = True
        Current_Sheet.Range("T1:AB33").HorizontalAlignment = xlCenter
        Current_Sheet.Range("T1:AB33").VerticalAlignment = xlCenter
        Current_Sheet.Range("T1:AB33").Font.Size = 7
        Current_Sheet.Range("T1:AB1").Font.Color = vbBlue
        Current_Sheet.Range("T1:AB1").Font.Size = 8
        Current_Sheet.Range("T1:AB1").Font.Bold = True
        Current_Sheet.Range("T1:AB1") = "DAILY TOTALS"
        Current_Sheet.Range("T2:AB2").Interior.ColorIndex = 31
        Current_Sheet.Range("T2:AB2").Font.Color = vbBlue
        Current_Sheet.Range("T2:AB2").Font.Size = 8
        Current_Sheet.Range("T2:AB2").Font.Bold = True
        Current_Sheet.Range("T2") = "DAY"
        Current_Sheet.Range("U2") = "SRT-D"
        Current_Sheet.Range("V2") = "SRT-N"
        Current_Sheet.Range("W2") = "HRS-D"
        Current_Sheet.Range("X2") = "HRS-N"
        Current_Sheet.Range("Y2") = "APPROACHES"
        Current_Sheet.Range("Z2") = "LANDINGS"
        Current_Sheet.Range("AA2") = "PAX"
        Current_Sheet.Range("AB2") = "CARGO"
        
        ' Color the Borders of the areas we will be working in
        Range("A1:AB33").Select
        Selection.Borders(xlDiagonalDown).LineStyle = xlNone
        Selection.Borders(xlDiagonalUp).LineStyle = xlNone
        For j = 0 To 4
            With Selection.Borders(j)
                .LineStyle = xlContinuous
                .ColorIndex = 4
                .Weight = xlThin
            End With
        Next j
        
        ' Autofit the columns so that the Header is more readable
        .Range("A1:AB33").Select
        Selection.Columns.AutoFit
        .Range("A1").Select
        
        ' Calculate the Totals for each day
        Call Daily_Totals
    End With
Exit_Point:
End Sub

' Gotta go Fast... SONIC!  Gotta go Fast... SONIC! Gotta go Faster-Faster, FASTER-FASTER-FASTER!?
Private Sub Go_Fasters()
    
    ' [A] : Application Object
    Dim A As Object
    Set A = Application
    A.Calculation = xlAutomatic
    
    ' Store default settings inside of an array
    Toggle_Option = Array(A.ScreenUpdating, A.DisplayStatusBar, A.Calculation, A.EnableEvents)
    
    ' Turn off unnecassary functions
    Application.ScreenUpdating = Not Toggle_Option(0)
    Application.DisplayStatusBar = Not Toggle_Option(1)
    Application.Calculation = xlCalculationManual
    Application.EnableEvents = Not Toggle_Option(3)
End Sub

' Configure settings to their original state
Private Sub Restore()
    
    ' If "Go_Fasters" has not been called, then exit-- for values are already default
    On Error GoTo ExitPoint
    
    ' Restore default settings
    Application.ScreenUpdating = Toggle_Option(0)
    Application.DisplayStatusBar = Toggle_Option(1)
    Application.Calculation = Toggle_Option(2)
    Application.EnableEvents = Toggle_Option(3)

ExitPoint:
End Sub
