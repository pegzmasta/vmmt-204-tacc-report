Rem Attribute VBA_ModuleType=VBAModule
Option VBASupport 1
' This procedure will import the Unit Flights file as the last sheet in Thisworkbook
Sub Import_File()
    Dim Wkb As Workbook, Wks As Worksheet
    Dim Msg As String, Response As String, Unit_Flights As String
    Dim Last_Cell As Integer
    
    ' Make Excel Calculate Faster
    Call Go_Fasters
    
    ' Prompt the User to download the Unit Flights file from M-SHARP
    Msg = "Please select the " & Chr(34) & "Unit Flights - Expanded" & Chr(34) & " file that you downloaded from M-SHARP."
    Response = MsgBox(Msg, vbOKOnly + vbInformation, "Importing Unit Flights to Smart Charlie")
    
    ' Now call the Open Dialog Box for filesystem navigation
    Unit_Flights = Application.GetOpenFilename(, , "Select Unit Flights File")
    
    ' Provide an escape in case the User presses "Cancel"
    If Unit_Flights <> "False" And Unit_Flights <> "" Then
        
        ' Open the Unit Flights file
        Set Wkb = Workbooks.Open(Unit_Flights): Set Wks = Wkb.Sheets(1)
        
        ' Sort the Sheet so that only our Units' Flights appear, and add it to a new sheet; Criteria1 utilizes our UnitID
        Last_Cell = Range("A7").End(xlDown).Row
        Rows("6:6").Select
        Selection.AutoFilter
        ActiveWindow.SmallScroll Down:=-6
        ActiveSheet.Range("$A$6:$DI$" & Last_Cell).AutoFilter Field:=5, Criteria1:="M01162"
        Cells.Select
        Range("A6").Activate
        Selection.Copy
        Sheets.Add After:=Sheets(Sheets.count)
        ActiveSheet.Paste
        Application.CutCopyMode = False

        ' Import the new sheet into Thisworkbook
        Sheets("Sheet1").Move After:=ThisWorkbook.Sheets(ThisWorkbook.Worksheets.count)
        Wkb.Close savechanges:=False
        ActiveWindow.Zoom = 70
        ThisWorkbook.Sheets(1).Activate
    Else: Exit Sub
    End If
    
    ' Restore orginal Settings
    Call Restore
End Sub

' Gotta go Fast... SONIC!  Gotta go Fast... SONIC! Gotta go Faster-Faster, FASTER-FASTER-FASTER!?
Private Sub Go_Fasters()
    
    ' [A] : Application Object
    Dim A As Object
    Set A = Application
    A.Calculation = xlAutomatic
    
    ' Store default settings inside of an array
    Toggle_Option = Array(A.ScreenUpdating, A.DisplayStatusBar, A.Calculation, A.EnableEvents)
    
    ' Turn off unnecassary functions
    Application.ScreenUpdating = Not Toggle_Option(0)
    Application.DisplayStatusBar = Not Toggle_Option(1)
    Application.Calculation = xlCalculationManual
    Application.EnableEvents = Not Toggle_Option(3)
End Sub

' Configure settings to their original state
Private Sub Restore()
    
    ' If "Go_Fasters" has not been called, then exit-- for values are already default
    On Error GoTo ExitPoint
    
    ' Restore default settings
    Application.ScreenUpdating = Toggle_Option(0)
    Application.DisplayStatusBar = Toggle_Option(1)
    Application.Calculation = Toggle_Option(2)
    Application.EnableEvents = Toggle_Option(3)

ExitPoint:
End Sub