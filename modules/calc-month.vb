Rem Attribute VBA_ModuleType=VBAModule
Option VBASupport 1
' Calculate The values for each category of each day for the month
Sub Daily_Totals()
    Dim Wkb As Workbook
    Dim Wks As Worksheet
    Dim xStart As Integer, xLast As Integer, i As Integer, j As Integer, count As Integer, Days() As Integer
    Dim last_day As Integer, d As Integer
    
    ' Make Excel Calculate Faster
    Call Go_Fasters

    ' Calculate the month on the current sheet
    Set Wks = ThisWorkbook.ActiveSheet
    
    ' Exit the moment an error occurs
    On Error GoTo Exit_Point
    With Wks
        xStart = .Range("A3").Row
        xLast = .Range("A3").End(xlDown).Row
        Wks.Range("U3:AB33").ClearContents: Rem-- Erase current values; otherwise numbers will be added on top of old values...
        ReDim Days(xLast)

        ' Collect the days which contain values, and store them in an array
        For i = xStart To xLast
            If i = 3 Then
                Days(0) = .Range("A" & i) * 1
            Else
                For j = LBound(Days) To UBound(Days)
                    If Days(j) = 0 Then
                        If Days(j - 1) <> .Range("A" & i) * 1 Then
                            Days(j) = .Range("A" & i) * 1: Exit For
                        Else
                            If Days(j - 1) = .Range("A" & i) * 1 Then: Exit For
                        End If
                    End If
                Next j
            End If
        Next i
        
        ' Enumerate the spreadsheet with the days in the array-- skipping any days which contain zero values
        For i = LBound(Days) To UBound(Days)
            If Days(i) <> 0 Then: .Range("T" & 2 + Days(i)) = Days(i)
        Next i
        d = 1
        count = 1
        
        ' Calculate the sums of the values for each given day; then, move on to the next day
        For i = LBound(Days) To UBound(Days)
            If d <= 0 Then: Exit For
            If .Range("A" & i + 3) = "" Then: Exit For
            If .Range("A" & i + 3) <> d Then: d = .Range("A" & i + 3)
            Rem-- Debug.Print "A" & .Range("A" & i + 3).Row, "Day = " & d, "Days(i) = Days(" & i & ") = " & Days(i)
            Rem-- Debug.Assert (d <> 8)
            If .Range("A" & i + 3) * 1 = .Range("T" & 2 + d) Then
                last_day = .Range("P" & 2 + Days(i)).Row
                .Range("U" & 2 + d) = .Range("U" & 2 + d) + .Range("G" & i + 3)
                .Range("V" & 2 + d) = .Range("V" & 2 + d) + .Range("H" & i + 3)
                .Range("W" & 2 + d) = .Range("W" & 2 + d) + .Range("L" & i + 3)
                .Range("X" & 2 + d) = .Range("X" & 2 + d) + .Range("M" & i + 3)
                .Range("Y" & 2 + d) = .Range("Y" & 2 + d) + .Range("P" & i + 3)
                .Range("Z" & 2 + d) = .Range("Z" & 2 + d) + .Range("Q" & i + 3)
                .Range("AA" & 2 + d) = .Range("AA" & 2 + d) + .Range("R" & i + 3)
                .Range("AB" & 2 + d) = .Range("AB" & 2 + d) + .Range("S" & i + 3)
                count = count + 1
                ElseIf .Range("A" & i + 2) * 1 = .Range("T" & last_day) Then
                    .Range("U" & last_day) = .Range("U" & last_day) + .Range("G" & i + 3)
                    .Range("V" & last_day) = .Range("V" & last_day) + .Range("H" & i + 3)
                    .Range("W" & last_day) = .Range("W" & last_day) + .Range("L" & i + 3)
                    .Range("X" & last_day) = .Range("X" & last_day) + .Range("M" & i + 3)
                    .Range("Y" & last_day) = .Range("Y" & last_day) + .Range("P" & i + 3)
                    .Range("Z" & last_day) = .Range("Z" & last_day) + .Range("Q" & i + 3)
                    .Range("AA" & last_day) = .Range("AA" & last_day) + .Range("R" & i + 3)
                    .Range("AB" & last_day) = .Range("AB" & last_day) + .Range("S" & i + 3)
                    count = count + 1
                    ElseIf .Range("A" & i + 2) * 1 = .Range("T" & last_day + 1) Then
                        .Range("U" & last_day + 1) = .Range("U" & last_day + 1) + .Range("G" & i + 3)
                        .Range("V" & last_day + 1) = .Range("V" & last_day + 1) + .Range("H" & i + 3)
                        .Range("W" & last_day + 1) = .Range("W" & last_day + 1) + .Range("L" & i + 3)
                        .Range("X" & last_day + 1) = .Range("X" & last_day + 1) + .Range("M" & i + 3)
                        .Range("Y" & last_day + 1) = .Range("Y" & last_day + 1) + .Range("P" & i + 3)
                        .Range("Z" & last_day + 1) = .Range("Z" & last_day + 1) + .Range("Q" & i + 3)
                        .Range("AA" & last_day + 1) = .Range("AA" & last_day + 1) + .Range("R" & i + 3)
                        .Range("AB" & last_day + 1) = .Range("AB" & last_day + 1) + .Range("S" & i + 3)
                        count = last_day + 1
            Else
                count = count + 1
                i = i - 1
            End If
        Next i
        
        ' Fill in any days' categories, that contain no values, with zeroes
        For i = 1 To d
            If .Range("T" & i + 2) <> i Then
                .Range("T" & i + 2) = i
                .Range("U" & i + 2) = 0
                .Range("V" & i + 2) = 0
                .Range("W" & i + 2) = 0
                .Range("X" & i + 2) = 0
                .Range("Y" & i + 2) = 0
                .Range("Z" & i + 2) = 0
                .Range("AA" & i + 2) = 0
                .Range("AB" & i + 2) = 0
            End If
        Next i
        
        ' Restore orginal Settings
        Call Restore
    End With
Exit_Point:
End Sub

' Gotta go Fast... SONIC!  Gotta go Fast... SONIC! Gotta go Faster-Faster, FASTER-FASTER-FASTER!?
Private Sub Go_Fasters()
    
    ' [A] : Application Object
    Dim A As Object
    Set A = Application
    A.Calculation = xlAutomatic
    
    ' Store default settings inside of an array
    Toggle_Option = Array(A.ScreenUpdating, A.DisplayStatusBar, A.Calculation, A.EnableEvents)
    
    ' Turn off unnecassary functions
    Application.ScreenUpdating = Not Toggle_Option(0)
    Application.DisplayStatusBar = Not Toggle_Option(1)
    Application.Calculation = xlCalculationManual
    Application.EnableEvents = Not Toggle_Option(3)
End Sub

' Configure settings to their original state
Private Sub Restore()
    
    ' If "Go_Fasters" has not been called, then exit-- for values are already default
    On Error GoTo ExitPoint
    
    ' Restore default settings
    Application.ScreenUpdating = Toggle_Option(0)
    Application.DisplayStatusBar = Toggle_Option(1)
    Application.Calculation = Toggle_Option(2)
    Application.EnableEvents = Toggle_Option(3)

ExitPoint:
End Sub
